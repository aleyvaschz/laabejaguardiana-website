export default class LibraryServices {
	constructor () {

	}

	getLibrary() {
		let library = require('./library')
		return new Promise((resolve) => {
			resolve(library)
		})
	}
}
